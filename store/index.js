export const state = () => ({
  search: '',
  page: 1,
  baseUrl: 'https://swapi.co/api/starships/'
})

export const mutations = {
  SET_SEARCH(state, value) {
    state.search = value
  },
  SET_PAGE(state, value) {
    state.page = value
  }
}
