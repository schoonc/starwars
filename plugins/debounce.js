export default function debounce(func, time) {
  let timeoutId
  return function(...args) {
    clearTimeout(timeoutId)
    timeoutId = setTimeout(() => {
      func.call(this, ...args)
    }, time)
  }
}
